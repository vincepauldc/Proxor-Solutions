import java.io.FileNotFoundException;
import java.io.IOException;
import com.csvreader.CsvReader;

public class ReadFileWriteToConsole {
	
	private static final int MAXROW = 3;
	private static final int MAXCOL = 3;
    private static final String inFile = "in.csv";
	   
    public String[][] sheet = new String[MAXROW][MAXCOL];
	   
    public static void main(String args[]) throws IOException {
    	// create a ReadWriteToConsole object
    	//  Do not change the signature of this method.
    	// ... insert code here ...
    	// invoke readSheet()
    	// ... insert code here ...
    	// invoke writeSheet()
    	// ... insert code here ...
    	ReadFileWriteToConsole r = new ReadFileWriteToConsole();
    	r.readSheet();
    	r.writeSheet();
    }	
	   
    public void readSheet() throws IOException {
    	// ... insert code here ...
    	//  Do not change the signature of this method.
    	CsvReader cr = new CsvReader(ReadFileWriteToConsole.inFile);

    	for (int row = 0; row < sheet.length; row++) {
    		cr.readRecord();
			for (int col = 0; col < sheet.length; col++) {
				this.sheet[row][col] = cr.get(col);
			}
		}
	}
	   
	public void writeSheet(){
		// ... insert code here ...
    	//  Do not change the signature of this method.\
		for (int row = 0; row < sheet.length; row++) {
			for (int col = 0; col < sheet.length; col++) {
				System.out.print("[" + this.sheet[row][col] + "]");
			}
			System.out.println();
		}
	}
}
