import java.io.FileNotFoundException;
import java.io.IOException;

import com.csvreader.CsvReader;


public class ReadFileWriteToConsole {

    private static final String inFile = "in.csv";
    private CsvReader cr;
    private String[][] sheet;
    private int rowCount = 0;
    private int colCount = 0;

    public static void main(String args[]) throws IOException {
   	// (add code to implement the following)
   	// create a ReadFileWriteToConsole object
    // call method below to read the data from inFile
    // call method below to write the data to the console
    // Do not change the signature of this method.
    	ReadFileWriteToConsole r = new ReadFileWriteToConsole();
    	r.writeSheet();
    	return;
    }
	
    public ReadFileWriteToConsole() {
    	this.init();
    }

    public String getCell(int row, int col) {
    // return the value of the spreadsheet at the given row and column
    // Do not change the signature of this method.
    	if (row > this.getRowCount()) {
    		throw new Error("Row index is bigger than row size");
    	}
    	if (row > this.getColCount()) {
    		throw new Error("Col index is bigger than col size");
    	}
    	return this.sheet[row][col];
    }
	
    public int getRowCount() {
	// return the number of rows in the spreadsheet
	// Do not change the signature of this method.
    	return this.rowCount;
    }
    
    public int getColCount() {
	// return the number of columns in row
	// Do not change the signature of this method.
    	return this.colCount;
    }
    
    public void init() {
    	try {
        	this.makeSheet();
        	while (this.cr.readRecord()) {
        		this.rowCount++;
        	}
        	
        	this.makeSheet();
        	this.cr.readRecord();
        	this.colCount = this.cr.getColumnCount();
        	
        	this.makeSheet();
        	
        	this.sheet = new String[this.getRowCount()][this.getColCount()];
        	
        	for (int row = 0; row < this.getRowCount(); row++) {
        		this.cr.readRecord();
    			for (int col = 0; col < this.getColCount(); col++) {
    				this.sheet[row][col] = this.cr.get(col);
    			}
    		}			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	return;

    }
	  
    public void makeSheet() throws FileNotFoundException {
	// read the data from inFile
	// Do not change the signature of this method.
    	this.cr = new CsvReader(ReadFileWriteToConsole.inFile);
    }	
	   
    public void writeSheet() {
	// format data to console
	// Do not change the signature of this method.
    	for (int row = 0; row < this.getRowCount(); row++) {
			for (int col = 0; col < this.getColCount(); col++) {
				System.out.print("[" + getCell(row, col) + "]");
			}
			System.out.println();
		}
    	return;
    }
}
